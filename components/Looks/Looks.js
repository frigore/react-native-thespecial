import React from 'react';
import { FlatList, ScrollView, ImageEditor, StyleSheet, Text, Image } from 'react-native';
import Look from './Look'
//const stub = 'http://dev.thespecial.ru:8096/assets/images/look_images/1547494933_look29c0mp.jpg'
const stub = 'http://www.independentmediators.co.uk/wp-content/uploads/2016/02/placeholder-image.jpg'
export default class Looks extends React.Component {

    constructor()
    {
        super()
        this.state = {loaded: false}
    }

    likeLook = function(idx)
    {
        this.setState((state) => {
            state.data[idx].liked = !state.data[idx].liked
            return state
        })
    }.bind(this)

    foo = function(info)
    {
        //console.log(info)
        //console.log(this)
        this.setState({info: false})
        /*
        let begin = info.viewableItems[0].index - 7 < 0 ? 0 : info.viewableItems[0].index - 7
        let end = begin + 14
        */
       if(info.viewableItems.length === 0)
       {
           console.log('AYAYA')
           console.log(info)
           return
       }
        let begin = info.viewableItems[0].index === 0 ? info.viewableItems[0].index : info.viewableItems[0].index - 1
        let end = begin + 12
        
        console.log(`begin is ${begin}`)
        this.setState({begin: begin, end: end})
        /*
        this.setState((state) => 
        {
            return {visible: state.data.slice(begin, end)}
        })
        */
        console.log(this.state)
    }.bind(this)

    componentDidUpdate()
    {
        //console.log('updated')
    }

    componentWillUnmount()
    {
        console.log('unmounted')
    }

    async componentDidMount()
    {
        let response = await fetch('http://dev.thespecial.ru:8096/api/api/get_user_looks?user_id=62') //61
        let data = JSON.parse(response._bodyText).data
        /*
        response = await fetch('http://dev.thespecial.ru:8096/api/api/get_user_looks?user_id=62')
        data.push(...(JSON.parse(response._bodyText).data))
        response = await fetch('http://dev.thespecial.ru:8096/api/api/get_user_looks?user_id=60')
        data.push(...(JSON.parse(response._bodyText).data))
        response = await fetch('http://dev.thespecial.ru:8096/api/api/get_user_looks?user_id=59')
        data.push(...(JSON.parse(response._bodyText).data))
        response = await fetch('http://dev.thespecial.ru:8096/api/api/get_user_looks?user_id=50')
        data.push(...(JSON.parse(response._bodyText).data))
        response = await fetch('http://dev.thespecial.ru:8096/api/api/get_user_looks?user_id=51')
        data.push(...(JSON.parse(response._bodyText).data))
        response = await fetch('http://dev.thespecial.ru:8096/api/api/get_user_looks?user_id=52')
        data.push(...(JSON.parse(response._bodyText).data))
        */
        data = data.map(item => {
            item.key = item.lookimage_id
            item.liked = false
            return item
        })
        console.log(data)
        let data2 = data.slice(0,3)
        this.setState({data: data })
        this.setState({visible: data2 })
        this.setState({info: true})
       let promises = data.map(item => {
           return Image.prefetch(item.look_image)
       })

       

       await Promise.all(promises)

       promises = data.map(item => {
        return Image.getSize(item.look_image, (width, height) => {
            item.coef = height/width
        })
    })
       
    await Promise.all(promises)

        console.log(await Image.queryCache(data.map(item => {
            return item.look_image
        })))
        this.setState({loaded: true})
        console.log(this.state)
        console.log(this.state.data[0])
        let kappa = this
        console.log(await ImageEditor.cropImage(kappa.state.data[0].look_image, {offset: {x:100, y: 100}, size: {width: 100, height:100}}, () => (kappa.state.data.look_image + 'a'), () => 0))
        console.log(this.state)
    }

    render() {
        if(this.state.loaded === false)
            return <Text>Loading...</Text>
        return <FlatList 
        onViewableItemsChanged={this.foo}
        data={this.state.data}
        renderItem={ ({item, index}) =>
        {

            //console.log(stub)
            //console.log(item)
            if(index >= this.state.begin && index <= this.state.end)
            return <Look likeLook={function(){
                console.log('like')
                this.likeLook(index)}.bind(this)} data={item} key={item.lookimage_id}></Look>
            else
            return <Look data={{coordinates:[], look_image: stub}} key={item.lookimage_id}></Look>
        }}>
        </FlatList>
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 15,
      backgroundColor: '#fff',
    },
  });