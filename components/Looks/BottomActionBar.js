import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native'
import {Icon} from 'react-native-elements'
export default class BottomActionBar extends React.Component 
{
    constructor()
    {
        super()
        this.state = {liked: false}
        //this.setState({liked: false})
    }

    _onPress = () => {
        this.setState((state) => 
        {
            return {liked: !state.liked}
        })
    }

    render() 
    {
    return (
        <View style={{justifyContent: 'space-between', flexDirection: 'row', marginHorizontal: 16, marginTop: 10, marginBottom: 30}}>          
            <Icon
            onPress={
                function(){
                    this.props.likeLook()
                    this.forceUpdate()
                }.bind(this)}
                name={this.props.data.liked ? 'favorite' : 'favorite-border'}
                size={32}
                type='material'
                color='#f8b6b7' />
            <Icon
                name='share'
                size={32}
                type='material'
                color='#909090' />
        </View>
    )
  }
}