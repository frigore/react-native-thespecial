import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import CircleAvatar from './CircleAvatar'
import {Icon} from 'react-native-elements'
export default class UserInfoBar extends React.Component 
{
    constructor()
    {
        super()
    }

    _onPressButton = () => {}

/*
<TouchableNativeFeedback
        onPress={this._onPressButton}
        background={TouchableNativeFeedback.SelectableBackground()}>
      <View>
      <Icon
  name='more-horiz'
  size={32}
  type='material'
  color='#909090' />
  </View>
    </TouchableNativeFeedback>
*/

    render() 
    {
    return (
        <View flexDirection={'row'} style={{justifyContent: 'space-between'}}>
          <View flexDirection={'row'} style={{paddingTop: 8, paddingBottom: 8, paddingLeft: 15}}>
            <CircleAvatar data={this.props.data} />
            <View>
              <Text style={{fontSize: 16, marginLeft: 10, marginTop: 5}}>{this.props.data.fname}</Text>
              <Text style={{color: '#b0b0b0' ,fontSize: 16, marginLeft: 10}}>80 дней назад</Text>
            </View>
          </View>
          <View style={{paddingTop: 20, paddingBottom: 8, paddingRight: 20}}>
            <Icon
                name='more-horiz'
                size={32}
                type='material'
                color='#909090' />
          </View>
        </View>
    )
  }
}