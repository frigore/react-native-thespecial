import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import UserInfoBar from './UserInfoBar'
import BottomActionBar from './BottomActionBar'
import LookImage from './LookImage'
export default class Looks extends React.Component {
  /*
    async componentDidMount()
    {
        async function bar()
        {
        return new Promise(function(resolve, reject)
            {
            setTimeout(function(){resolve(Math.random())}, 2000)	
            })
        }
        await bar()
        let response = await fetch('http://dev.thespecial.ru:8096/api/api/get_user_looks?user_id=61')
        console.log(JSON.parse(response._bodyText))
        console.log('mounted!')
    }
*/

/*
(
        <View>
          <View flexDirection={'row'}>
          <View
          style={{borderRadius: 50/ 2, borderColor: "#f8b6b7", borderWidth: 2.5}}>
          <Image
            resizeMethod={'resize'}
            style={{width: 50, height: 50, borderRadius: 50/ 2, borderColor: "#fff", borderWidth: 1.5}}
            source={{uri: this.props.data.profile_image}}/>
          </View>
          <Text>Here is some looks of user {this.props.data.fname}</Text>
          </View>
        <Image
            resizeMethod={'resize'}
            style={{width: '100%', height: 400}}
            source={{uri: this.props.data.look_image}}/>
        </View>
            
    )
*/

/*

(
      <View>
        <View>
          <View flexDirection={'row'} style={{paddingTop: 8, paddingBottom: 8, paddingLeft: 15}}>
          <View
          style={{width: 55, height: 55, borderRadius: 55/ 2, borderColor: "#f8b6b7", borderWidth: 2.5}}>
          <Image
            resizeMethod={'resize'}
            style={{width: 50, height: 50, borderRadius: 50/ 2, borderColor: "#fff", borderWidth: 2.5}}
            source={{uri: this.props.data.profile_image}}/>
          </View>
          <View>
          <Text style={{fontSize: 16, marginLeft: 10, marginTop: 5}}>{this.props.data.fname}</Text>
          <Text style={{color: '#b0b0b0' ,fontSize: 16, marginLeft: 10}}>80 дней назад</Text>
          </View>
          </View>
        <Image
            resizeMethod={'resize'}
            style={{width: '100%', height: 400}}
            source={{uri: this.props.data.look_image}}/>
        </View>
        </View>
            
    )

*/

    constructor()
    {
        super()
    }

    _onPress = () => {
      this.setState((state) => 
      {
          return {liked: !state.liked}
      })
    }

    render() 
    {
      //console.log('rerender')      
      //<View flexDirection={'row'} style={{backgroundColor: '#ff0000'}}>
      //profile_image
      //font-awesome
    return (
        <View>
          <UserInfoBar data={this.props.data}/>
          <LookImage point={{size: 16, oreol: 12}} data={this.props.data}/>
          <BottomActionBar likeLook={this.props.likeLook} data={this.props.data}/>            
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 15,
      backgroundColor: '#fff',
    },
  });