import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
export default class CircleAvatar extends React.Component 
{
    constructor()
    {
        super()
    }

    render() 
    {
      //console.log('rerender')      
      //<View flexDirection={'row'} style={{backgroundColor: '#ff0000'}}>
      //profile_image
    return (
        <View
        style={{width: 55, height: 55, borderRadius: 55/ 2, borderColor: "#f8b6b7", borderWidth: 2.5}}>
            <Image
            resizeMethod={'resize'}
            style={{width: 50, height: 50, borderRadius: 50/ 2, borderColor: "#fff", borderWidth: 2.5}}
            source={{uri: this.props.data.profile_image}}/>
        </View>
    )
  }
}