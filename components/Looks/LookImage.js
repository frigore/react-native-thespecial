import React from 'react';
import { View, StyleSheet, Text, Image, Dimensions } from 'react-native';
import {Icon} from 'react-native-elements'

/*
            <Icon
              iconStyle={{alignSelf: 'flex-start', transform: [{translateX: -16 + 400*0.5}, {translateY: -416}]}}
                name='circle'
                size={32}
                type='font-awesome'
                color='red' />
                */

export default class LookImage extends React.Component 
{
    constructor(props)
    {
        super(props)
    }

    get point()
    {
        if(this.props.point)
            return this.props.point
        else
        {
            let point = {size: 32, oreol: 16}
            return point
        }
    }

    get size()
    {
      return {
        width: Dimensions.get('window').width,
        height: 400
      }
    }

    get coords()
    {
        let coordinates = []
        console.log(`coef is ${this.props.data.coef}`)
        this.props.data.coordinates.forEach((coord) => {
            let result = Object.assign({}, coord)
            result.left = Number(coord.left.replace(/%/, '') / 100)
            result.top = Number(coord.top.replace(/%/, '') / 100)
            coordinates.push(result)
        })
        /*
        coordinates.push({left: 0, top: 0, itemid: '1010101'}, 
          {left: 0.25, top: 0.25, itemid: '1010102'}, 
          {left: 0.5, top: 0.5, itemid: '1010103'}, 
          {left: 0.75, top: 0.75, itemid: '1010104'}, 
          {left: 1, top: 1, itemid: '1010105'} )
        console.log(coordinates)
          */

        coordinates.push({left: 0, top: 0, itemid: '1010101'}, 
        {left: 0, top: 1, itemid: '1010102'}, 
        {left: 1, top: 0, itemid: '1010103'}, 
        {left: 1, top: 1, itemid: '1010104'})
        return coordinates
    }

    get marker()
    {
      return (
        <View>
        <Icon
          name='circle'
          size={this.point.size + this.point.oreol}
          type='font-awesome'
          color='rgba(238, 182, 183, 0.4)' />
        <Icon                  
          iconStyle={{transform: [{translateY: -this.point.size -(this.point.oreol)/2}]}}
          name='circle'
          size={this.point.size}
          type='font-awesome'
          color='rgba(238, 182, 183, 0.8)' />
        </View>
      )
    }

    render() 
    {
      let coef = this.props.data.coef
      if(!this.props.data.coef)
        coef = 1

      let imgwidth = this.size.width
      let imgheight = imgwidth * coef

      if(this.size.height / this.size.width > coef)
      {
        imgheight = this.size.height
        imgwidth = imgheight / (coef)
      }
        console.log({imgheight, imgwidth, height: this.size.height, width: this.size.width, coef})
    return (
        <View style={{width: this.size.width, height: this.size.height}}>
        <Image
              style={{width: this.size.width, height: this.size.height}}
              source={{uri: this.props.data.look_image}}/>

              {this.coords.map((coord, index) => {
                let width = this.size.width
                let height = width * coef
                let offset = (height - this.size.height)/2
                let transform = [
                  {translateX: -(this.point.size + this.point.oreol)/2 + width*coord.left},
                  {translateY: -(this.point.size + this.point.oreol)*(index + 0.5) - offset - this.size.height + height*(coord.top)}]
                  if(this.size.height / this.size.width > coef)
                  {
                    
                    let height = this.size.height
                    let width = height / coef
                    let offset = (width - this.size.width)/2
                    transform = [
                      {translateX: -(this.point.size + this.point.oreol)/2 - offset + width*coord.left},
                      {translateY: -(this.point.size + this.point.oreol)*(index + 0.5) + height*(coord.top - 1)}]
                  }
                  return (
                  <View key={coord.itemid} style={{ width: (this.point.size + this.point.oreol), height: (this.point.size + this.point.oreol), alignSelf: 'flex-start', transform}}>
                    {this.marker}                  
                    </View>
                    )
              })}
        </View>
    )
  }
}

/*

<Image
              resizeMethod={'resize'}
              style={{transform: [{translateY: -this.coords.length * (this.point.size + this.point.oreol) - this.size.height - (imgheight-this.size.height)/2} ], opacity: 0.5, width: imgwidth, height: imgheight}}
              source={{uri: this.props.data.look_image}}/>

<Icon
                  key={coord.itemid}
                  iconStyle={{transform: [{translateX: -(this.point.size + this.point.oreol)/2}, {translateY: -(this.point.size + this.point.oreol)/2}]}}
                    name='circle'
                    size={this.point.size}
                    type='font-awesome'
                    color='rgba(238, 182, 183, 0.8)' />

*/