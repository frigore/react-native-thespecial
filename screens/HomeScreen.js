import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Button,
  AsyncStorage
} from 'react-native';
import { WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props)
  {
    super(props)
    this.state = { text: '', value: 0, refreshing: false };
  }

  

  _reload = async() => {
    async function bar()
    {
      return new Promise(function(resolve, reject)
        {
        setTimeout(function(){resolve(Math.random())}, 2000)	
        })
    }
    /*
    console.log('qwerop')
    console.log(this)
    */
    
    this.setState({refreshing: true})
    let value = await bar()
    this.setState({refreshing: false})
    this.setState({value: value})
    //console.log(this.state)
  }

  render() {
    let value = 1
    async function bar()
    {
      return new Promise(function(resolve, reject)
        {
        setTimeout(function(){resolve(Math.random())}, 5000)	
        })
    }
    //console.log('alslsl')
    let kappa = this
    async function zappa()
    {
      //console.log(kappa.state)
      //console.log('aoaoao')
      let value = await bar()
      //value = await AsyncStorage.getItem('@MySuperStore:key')
      kappa.setState(before => {
        before.value = value
        return before
      })
      //console.log(kappa.state)
    }
    //zappa()
    //console.log(Object.assign(styles.getStartedText, {color: 'rgb(255,0,0)'}))
    //console.log(Object.assign({color: 'rgb(255,0,0)'}, styles.getStartedText))
    //console.log(Object.assign(Object.assign({}, styles.getStartedText), {color: 'rgb(255,0,0)'}))
    let luminance = Math.floor(255 * this.state.value)
    let tag = <Text style={Object.assign(Object.assign({}, styles.getStartedText), {color: `rgb(${luminance},${luminance},${luminance})`})}>
    Super awesome!
  </Text>
    return (
      <View style={styles.container}>        
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>

        <View>
          <Text>Hey there</Text>
        </View>
          <View style={styles.welcomeContainer}>
            <Image
              source={
                __DEV__
                  ? require('../assets/images/robot-dev.png')
                  : require('../assets/images/robot-prod.png')
              }
              style={styles.welcomeImage}
            />
            <Button 
            title={'Go somewhere 2'}
            onPress={() => this.props.navigation.navigate('Links')}
            >
            </Button>
          </View>

          <View style={styles.getStartedContainer}>
            {this._maybeRenderDevelopmentModeWarning()}

            <Text style={styles.getStartedText}>Kappa123</Text>

            <View style={[styles.codeHighlightContainer, styles.homeScreenFilename]}>              
              <MonoText style={styles.codeHighlightText}>Lorem Ipsum</MonoText>
            </View>

            <Text style={styles.getStartedText}>
              Dolor sit amet
              {this.state.value}
            </Text>
            {tag}
            <TextInput
              placeholder="Type here to translate!"
              style={{height: 40, width: '100%', borderColor: 'gray', borderWidth: 1}}
              onChangeText={(text) => this.setState({text})}
              value={this.state.text}
            />
            <Text>{this.state.text}</Text>
          </View>

          <View style={styles.helpContainer}>
            <TouchableOpacity onPress={this._handleHelpPress} style={styles.helpLink}>
              <Text style={styles.helpLinkText}>Help, it didn’t automatically reload!</Text>
            </TouchableOpacity>
            <Text style={styles.getStartedText}>
              Dolor sit amet
            </Text>
          </View>
          <ScrollView refreshControl={
        <RefreshControl onRefresh={this._reload} colors={['red', 'green', 'blue']} refreshing={this.state.refreshing} />}>
        <Text>Hey there</Text>
        <Text>Hey there</Text>
        <Text>Hey there</Text>
        <Text>Hey there</Text>
        </ScrollView>

        </ScrollView>

        <View style={styles.tabBarInfoContainer}>
          <Text style={styles.tabBarInfoText}>This is a tab bar. You can edit it in:</Text>

          <View style={[styles.codeHighlightContainer, styles.navigationFilename]}>
            <MonoText style={styles.codeHighlightText}>navigation/MainTabNavigator.js</MonoText>
          </View>
        </View>
      </View>
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
