import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import LooksScreen from '../screens/LooksScreen';
import {Icon} from 'react-native-elements'


const HomeStack = createStackNavigator({
  Home: LooksScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused, horizontal, tintColor }) => (
    <Icon
      size={36}
      name='home'
      type='material'
      color={tintColor} />
  ),
  tabBarOptions: {
    showLabel: false,
    activeTintColor: 'pink',
    inactiveTintColor: 'gray',
  },
};

const LinksStack = createStackNavigator({
  Links: LinksScreen,
});

LinksStack.navigationOptions = {
  tabBarLabel: 'Links',
  tabBarIcon: ({ focused, horizontal, tintColor }) => (
    <Icon
      size={36}
      name='search'
      type='material'
      color={tintColor} />
  ),
  tabBarOptions: {
    showLabel: false,
    activeTintColor: 'pink',
    inactiveTintColor: 'gray',
  },
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused, horizontal, tintColor }) => (
    <Icon
      size={36}   
      name='add-circle-outline'
      type='material'
      color={tintColor} />
  ),
  tabBarOptions: {
    showLabel: false,
    activeTintColor: 'pink',
    inactiveTintColor: 'gray',
  },
};

const UsersStack = createStackNavigator({
  Looks: HomeScreen,
});

UsersStack.navigationOptions = {
  tabBarLabel: 'Looks',
  tabBarIcon: ({ focused, horizontal, tintColor }) => (
    <Icon
      size={36}
      name='person-outline'
      type='material'
      color={tintColor} />
  ),
  tabBarOptions: {
    showLabel: false,
    activeTintColor: 'pink',
    inactiveTintColor: 'gray',
  },
}

export default createBottomTabNavigator({
  HomeStack,
  LinksStack,
  SettingsStack,
  UsersStack
});
